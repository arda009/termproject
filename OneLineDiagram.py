import json
import os.path
from worldmap import worldmap
from Methods import draw
import csv #csv dosyasını okumak için
from tkinter.filedialog import askopenfilename
CSVname=askopenfilename()
f= open(CSVname)
csv_f=csv.reader(f)
b1name=[]
for row in csv_f:
    b1name.append(row)
f.close()
B1=""
B2=""
B3=""
station=worldmap.wmap
B3ElementDict = {}
B2array={}
for i in range(len(b1name) - 1): #2 dictionary oluşturuyor B2 ve B3 için
    tmp = str(b1name[i + 1])
    x = tmp.split(";")
    B1F = str(x[0])
    B1 = B1F[2:len(B1F)]
    B2=str(x[1])
    B3 = str(x[2])
    element = str(x[4])
    ASDU=str(x[3])
    if B3 in B3ElementDict:
        B3ElementDict[B3].append(element)
    else:
        B3ElementDict[B3] = [element]
        if B2 in B2array:
            B2array[B2].append(B3)
        else:
            B2array[B2]=[B3]
fiderSayisi=0

for b3 in B3ElementDict: #fider sayma algoritması
    if("Iso Bb 1" in B3ElementDict[b3] or "CB" in B3ElementDict[b3] or "Iso Line" in B3ElementDict[b3] or "Iso Eth1" in B3ElementDict[b3] or "LBS" in B3ElementDict):
        fiderSayisi += 1

fidNo = 0
fiderAralıgı = 500
viewNo = "1358"
viewHeight = 4000
viewX = 0
viewY = 4500
viewWidth = 4000 + fiderSayisi*500
view = draw.viewMaker(worldmap.view, str(viewWidth), str(viewHeight), "wednesday", viewNo, str(viewX), str(viewY))
station = station + worldmap.bosluk +view
x = int(viewX + viewWidth/2 - ((fiderSayisi-1) * fiderAralıgı / 2 + 200))
y = int(viewY + viewHeight/2) + 350
for b2 in B2array:
    for b3 in B3ElementDict:
        if b3 in B2array[b2]:
            bara = draw.baraMaker(B1, b2, b3, fiderSayisi, x, y)
            station = station + worldmap.bosluk + bara


            if ("Iso Bb 1" in B3ElementDict[b3]):
                if ("CB" in B3ElementDict[b3]):
                    if ("Iso Eth1" in B3ElementDict[b3]):
                        fidNo += 1
                        station= station + worldmap.bosluk + draw.kesiciAyiriciToprak(B1, b2, b3, x + fiderAralıgı*(fidNo -1), y, B3ElementDict[b3] )
                    elif("Iso Line" in B3ElementDict[b3]):
                        fidNo += 1
                        station = station + worldmap.bosluk + draw.kesiciAyiriciHat(B1, b2, b3, x + fiderAralıgı * (fidNo - 1), y, B3ElementDict[b3])
                    else:
                        fidNo += 1
                        station = station + worldmap.bosluk + draw.kesiciAyirici(B1, b2, b3, x + fiderAralıgı * (fidNo - 1), y, B3ElementDict[b3])
                elif ("Iso Eth1" in B3ElementDict[b3]):
                    fidNo += 1
                    station = station + worldmap.bosluk + draw.baraToprak(B1, b2, b3, x + fiderAralıgı * (fidNo - 1), y, B3ElementDict[b3])
                else:
                    fidNo += 1
                    station = station + worldmap.bosluk + draw.bara(B1, b2, b3, x + fiderAralıgı * (fidNo - 1), y , B3ElementDict[b3])
            elif ("LBS" in B3ElementDict[b3] and "Iso Eth1" in B3ElementDict[b3]):
                fidNo += 1
                station = station + worldmap.bosluk + draw.LBSmaker(B1, b2, b3, x + fiderAralıgı * (fidNo - 1), y)
station = station + draw.butonB1Maker(x + 200+(fiderSayisi-1)*250 - 672 + 328, y - 1670, B1)
B2 = list(B2array)[0]
B3 = list(B3ElementDict)[0]
"""with open('configler.json') as data_file:
    data = json.load(data_file)
if(data["KuplajTuru"]=="ikili"):
    print(data["KuplajYeri1"] + " ve " + data["KuplajYeri2"] + " arası kuplaj")"""



B3 = list(B3ElementDict)[1]
'''
isoBB= draw.isoBBmaker(B1,B2,B3, x+200-41, y+65 )
station = station + worldmap.bosluk + isoBB
topo1= draw.topoMaker(B1,B2,B3, x+200, y+65+82 )
station = station + worldmap.bosluk + topo1

cb= draw.cbMaker(B1,B2,B3, x+200-41, y+65+82+114 )
station = station + worldmap.bosluk + cb

topo2_1= draw.topo2_1Maker(B1,B2,B3, x+200, y+65+82+114+82 )
station = station + worldmap.bosluk + topo2_1

isoEth= draw.isoEthMaker(B1,B2,B3, x+200+64, y+65+82+114+82+140-20 )
station = station + worldmap.bosluk + isoEth

I= draw.Imaker(B1,B2,B3, x + 200 - 135, y - 618 )
station = station + worldmap.bosluk + I

power = draw.powerMaker(B1,B2,B3, x + 200 - 135, y - 618 + 48 )
station = station + worldmap.bosluk + power

F_I = draw.F_Imaker(B1,B2,B3, x + 200 - 135, y - 618 - 260)
station = station + worldmap.bosluk + F_I

reset = draw.resetMaker(B1,B2,B3, x + 200 - 200, y - 618 - 96)
station = station + worldmap.bosluk + reset

senaryo = draw.senaryoMaker(B1,B2,B3, x + 200 - 200, y - 618 - 98 - 76)
station = station + worldmap.bosluk + senaryo

FeedName= draw.FeedNameMaker(B1,B2,B3, x+200 - 54, y - 100 )
station = station + worldmap.bosluk + FeedName
'''
station = station + """
			</Worldmap>
		</Parent>
	</Instances>
</XDF>"""
save_path='C:\\Users\z0041r0c\Desktop'
StationName=os.path.join(save_path,'Station_'+B1+'.xml')
fw=open(StationName, 'w')
fw.write(station)
fw.close()