from worldmap import worldmap
class finder:
    def eleFinder(feeder):
        if (feeder[0] == 'L') or (feeder[0] == 'L'):
            for a in range(len(feeder)):
                ele = feeder[a]
                if(ele[0:2] == "IS"):
                    if(ele[len(ele)-1] == "2"):
                        feeder[a] = "Iso Bb 1"
                    elif (ele[len(ele)-1] == "1"):
                        feeder[a] = "Iso Line"
                elif(ele[0:2] == "CB"):
                    feeder[a] = "CB"
        else:
            for a in range(len(feeder)):
                ele = feeder[a]
                if(ele[0:2] == "IS"):
                    if(ele[len(ele)-1] == "1"):
                        feeder[a] = "Iso Bb 1"
                    elif (ele[len(ele)-1] == "2"):
                        feeder[a] = "Iso Line"
                elif(ele[0:2] == "CB"):
                    feeder[a] = "CB"
        return feeder
    def feedersPlace(b3, fiderAralıgı, rSort, lSort, fidNo, busbarPlusCouplingWidth):
        if (b3[0] == 'R' or b3[0] == 'L'):
            if(b3[0] == 'R'):
                return (busbarPlusCouplingWidth + (fiderAralıgı * rSort.index(b3)))
            elif (b3[0] == 'L'):
                return fiderAralıgı * lSort.index(b3)
        else:
            return fiderAralıgı * (fidNo - 1)


class draw:
    resetWidth = 190
    resetHeight = 60
    ResetStartingY = 1220
    F_IStartingY = 980
    AnalogStartingY = 290
    PowerStartingY = 650
    CosPhiStartingY = 578
    AnaloglarArasiY = 72
    def viewMaker(view, width, height, name, no, x, y):
        view = view.replace("_viewName_", name)
        view = view.replace("_viewNo_", no)
        view = view.replace("_Genislik_", width)
        view = view.replace("_Yukseklik_", height)
        view = view.replace("_xCoord_", x)
        view = view.replace("_yCoord_", y)
        return view
    def baraMaker(b1, b2, b3, FiderSayisi, x, y, busNo):
        bara = worldmap.bara
        bbwith=400+(FiderSayisi-1)*500
        if (busNo == 'L'):
            b3 = "BB1"
        elif(busNo == 'R'):
            b3 = "BB2"

        bara = bara.replace("B1NAME", b1)
        bara = bara.replace("B2NAME", b2)
        bara = bara.replace("B3NAME", b3)
        bara = bara.replace("_Genislik_", str(bbwith))
        bara = bara.replace("_xCoord_", str(x))
        bara = bara.replace("_yCoord_", str(y))
        worldmap.number+=1
        bara = bara.replace("_Number", str(worldmap.number))
        for i in range(FiderSayisi):
            baraFid = worldmap.baraFid
            baraFid = baraFid.replace("B1NAME", b1)
            baraFid = baraFid.replace("B2NAME", b2)
            baraFid = baraFid.replace("B3NAME", b3)
            baraFid = baraFid.replace("_xCoord_", str(x + 200 + i*500))
            baraFid = baraFid.replace("_yCoord_", str(y))
            worldmap.number += 1
            baraFid = baraFid.replace("_Number", str(worldmap.number))
            bara = bara + worldmap.bosluk + baraFid
        return bara
    def isoBBmaker(b1, b2, b3, x, y):
        isoBB = worldmap.isoBB
        isoBBs = worldmap.isoBBs
        isoBB = isoBB.replace("B1NAME", b1)
        isoBB = isoBB.replace("B2NAME", b2)
        isoBB = isoBB.replace("B3NAME", b3)
        isoBB = isoBB.replace("_xCoord_", str(x))
        isoBB = isoBB.replace("_yCoord_", str(y))
        worldmap.number += 1
        isoBB = isoBB.replace("_Number", str(worldmap.number))

        isoBBs = isoBBs.replace("B1NAME", b1)
        isoBBs = isoBBs.replace("B2NAME", b2)
        isoBBs = isoBBs.replace("B3NAME", b3)
        worldmap.number += 1
        isoBBs = isoBBs.replace("_Number", str(worldmap.number))
        isoBBs = isoBBs.replace("_xCoord_", str(x-150))
        isoBBs = isoBBs.replace("_yCoord_", str(y+10))
        isoBB1 = isoBB +  worldmap.bosluk + isoBBs
        return isoBB1
    def topoMaker(b1, b2, b3, x, y):
        topo1 = worldmap.topo1
        topo1 = topo1.replace("B1NAME", b1)
        topo1 = topo1.replace("B2NAME", b2)
        topo1 = topo1.replace("B3NAME", b3)
        topo1 = topo1.replace("_xCoord_", str(x))
        topo1 = topo1.replace("_yCoord_", str(y))
        worldmap.number += 1
        topo1 = topo1.replace("_Number", str(worldmap.number))
        return topo1
    def cbMaker(b1, b2, b3, x, y):
        cb = worldmap.cb
        cbs = worldmap.cbs
        cb = cb.replace("B1NAME", b1)
        cb = cb.replace("B2NAME", b2)
        cb = cb.replace("B3NAME", b3)
        cb = cb.replace("_xCoord_", str(x))
        cb = cb.replace("_yCoord_", str(y))
        worldmap.number += 1
        cb = cb.replace("_Number", str(worldmap.number))

        cbs = cbs.replace("B1NAME", b1)
        cbs = cbs.replace("B2NAME", b2)
        cbs = cbs.replace("B3NAME", b3)
        worldmap.number += 1
        cbs = cbs.replace("_Number", str(worldmap.number))
        cbs = cbs.replace("_xCoord_", str(x - 150))
        cbs = cbs.replace("_yCoord_", str(y + 10))
        cb1 = cb + worldmap.bosluk + cbs
        return cb1
    def topo2_1Maker(b1, b2, b3, x, y):
        topo2_1_1 = worldmap.topo2_1_1
        topo2_1_1 = topo2_1_1.replace("B1NAME", b1)
        topo2_1_1 = topo2_1_1.replace("B2NAME", b2)
        topo2_1_1 = topo2_1_1.replace("B3NAME", b3)
        topo2_1_1 = topo2_1_1.replace("_xCoord_", str(x))
        topo2_1_1 = topo2_1_1.replace("_yCoord_", str(y))
        worldmap.number += 1
        topo2_1_1 = topo2_1_1.replace("_Number", str(worldmap.number))

        topo2_1_2 = worldmap.topo2_1_2
        topo2_1_2 = topo2_1_2.replace("B1NAME", b1)
        topo2_1_2 = topo2_1_2.replace("B2NAME", b2)
        topo2_1_2 = topo2_1_2.replace("B3NAME", b3)
        topo2_1_2 = topo2_1_2.replace("_xCoord_", str(x))
        topo2_1_2 = topo2_1_2.replace("_yCoord_", str(y+140))
        worldmap.number += 1
        topo2_1_2 = topo2_1_2.replace("_Number", str(worldmap.number))
        topo2_1 = topo2_1_1 + worldmap.bosluk + topo2_1_2
        return topo2_1
    def isoEthMaker(b1, b2, b3, x, y):
        isoEth = worldmap.isoEth
        isoEths = worldmap.isoEths
        isoEth = isoEth.replace("B1NAME", b1)
        isoEth = isoEth.replace("B2NAME", b2)
        isoEth = isoEth.replace("B3NAME", b3)
        isoEth = isoEth.replace("_xCoord_", str(x))
        isoEth = isoEth.replace("_yCoord_", str(y))
        worldmap.number += 1
        isoEth = isoEth.replace("_Number", str(worldmap.number))

        isoEths = isoEths.replace("B1NAME", b1)
        isoEths = isoEths.replace("B2NAME", b2)
        isoEths = isoEths.replace("B3NAME", b3)
        worldmap.number += 1
        isoEths = isoEths.replace("_Number", str(worldmap.number))
        isoEths = isoEths.replace("_xCoord_", str(x - 150 - 64))
        isoEths = isoEths.replace("_yCoord_", str(y + 10))
        isoEth1 = isoEth + worldmap.bosluk + isoEths
        return isoEth1
    def cosPhiMaker(b1, b2, b3, x, y):
        cosPhi_1 = worldmap.cosPhi_1
        cosPhi_1 = cosPhi_1.replace("B1NAME", b1)
        cosPhi_1 = cosPhi_1.replace("B2NAME", b2)
        cosPhi_1 = cosPhi_1.replace("B3NAME", b3)
        cosPhi_1 = cosPhi_1.replace("_xCoord_", str(x))
        cosPhi_1 = cosPhi_1.replace("_yCoord_", str(y))
        worldmap.number += 1
        cosPhi_1 = cosPhi_1.replace("_Number", str(worldmap.number))

        cosPhi_2 = worldmap.cosPhi_2
        cosPhi_2 = cosPhi_2.replace("B1NAME", b1)
        cosPhi_2 = cosPhi_2.replace("B2NAME", b2)
        cosPhi_2 = cosPhi_2.replace("B3NAME", b3)
        cosPhi_2 = cosPhi_2.replace("_xCoord_", str(x + 24))
        cosPhi_2 = cosPhi_2.replace("_yCoord_", str(y ))
        worldmap.number += 1
        cosPhi_2 = cosPhi_2.replace("_Number", str(worldmap.number))

        cosPhi_3 = worldmap.cosPhi_3
        cosPhi_3 = cosPhi_3.replace("B1NAME", b1)
        cosPhi_3 = cosPhi_3.replace("B2NAME", b2)
        cosPhi_3 = cosPhi_3.replace("B3NAME", b3)
        cosPhi_3 = cosPhi_3.replace("_xCoord_", str(x + 183))
        cosPhi_3 = cosPhi_3.replace("_yCoord_", str(y))
        worldmap.number += 1
        cosPhi_3 = cosPhi_3.replace("_Number", str(worldmap.number))

        cosPhi = cosPhi_1 + worldmap.bosluk + cosPhi_2 + worldmap.bosluk + cosPhi_3
        return cosPhi
    def Imaker(b1, b2, b3, x, y):
        I1_1 = worldmap.I1_1
        I1_1 = I1_1.replace("B1NAME", b1)
        I1_1 = I1_1.replace("B2NAME", b2)
        I1_1 = I1_1.replace("B3NAME", b3)
        I1_1 = I1_1.replace("_xCoord_", str(x))
        I1_1 = I1_1.replace("_yCoord_", str(y - draw.AnaloglarArasiY*3))
        worldmap.number += 1
        I1_1 = I1_1.replace("_Number", str(worldmap.number))

        I1_2 = worldmap.I1_2
        I1_2 = I1_2.replace("B1NAME", b1)
        I1_2 = I1_2.replace("B2NAME", b2)
        I1_2 = I1_2.replace("B3NAME", b3)
        I1_2 = I1_2.replace("_xCoord_", str(x+24))
        I1_2 = I1_2.replace("_yCoord_", str(y- draw.AnaloglarArasiY*3))
        worldmap.number += 1
        I1_2 = I1_2.replace("_Number", str(worldmap.number))

        I1_3 = worldmap.I1_3
        I1_3 = I1_3.replace("B1NAME", b1)
        I1_3 = I1_3.replace("B2NAME", b2)
        I1_3 = I1_3.replace("B3NAME", b3)
        I1_3 = I1_3.replace("_xCoord_", str(x+183))
        I1_3 = I1_3.replace("_yCoord_", str(y - draw.AnaloglarArasiY*3))
        worldmap.number += 1
        I1_3 = I1_3.replace("_Number", str(worldmap.number))

        I1 = I1_1 + worldmap.bosluk + I1_2 + worldmap.bosluk + I1_3

        I2_1 = worldmap.I2_1
        I2_1 = I2_1.replace("B1NAME", b1)
        I2_1 = I2_1.replace("B2NAME", b2)
        I2_1 = I2_1.replace("B3NAME", b3)
        I2_1 = I2_1.replace("_xCoord_", str(x))
        I2_1 = I2_1.replace("_yCoord_", str(y - draw.AnaloglarArasiY*2))
        worldmap.number += 1
        I2_1 = I2_1.replace("_Number", str(worldmap.number))

        I2_2 = worldmap.I2_2
        I2_2 = I2_2.replace("B1NAME", b1)
        I2_2 = I2_2.replace("B2NAME", b2)
        I2_2 = I2_2.replace("B3NAME", b3)
        I2_2 = I2_2.replace("_xCoord_", str(x + 24))
        I2_2 = I2_2.replace("_yCoord_", str(y - draw.AnaloglarArasiY*2))
        worldmap.number += 1
        I2_2 = I2_2.replace("_Number", str(worldmap.number))

        I2_3 = worldmap.I2_3
        I2_3 = I2_3.replace("B1NAME", b1)
        I2_3 = I2_3.replace("B2NAME", b2)
        I2_3 = I2_3.replace("B3NAME", b3)
        I2_3 = I2_3.replace("_xCoord_", str(x + 183))
        I2_3 = I2_3.replace("_yCoord_", str(y - draw.AnaloglarArasiY*2))
        worldmap.number += 1
        I2_3 = I2_3.replace("_Number", str(worldmap.number))

        I2 = I2_1 + worldmap.bosluk + I2_2 + worldmap.bosluk + I2_3

        I3_1 = worldmap.I3_1
        I3_1 = I3_1.replace("B1NAME", b1)
        I3_1 = I3_1.replace("B2NAME", b2)
        I3_1 = I3_1.replace("B3NAME", b3)
        I3_1 = I3_1.replace("_xCoord_", str(x))
        I3_1 = I3_1.replace("_yCoord_", str(y - draw.AnaloglarArasiY))
        worldmap.number += 1
        I3_1 = I3_1.replace("_Number", str(worldmap.number))

        I3_2 = worldmap.I3_2
        I3_2 = I3_2.replace("B1NAME", b1)
        I3_2 = I3_2.replace("B2NAME", b2)
        I3_2 = I3_2.replace("B3NAME", b3)
        I3_2 = I3_2.replace("_xCoord_", str(x + 24))
        I3_2 = I3_2.replace("_yCoord_", str(y - draw.AnaloglarArasiY))
        worldmap.number += 1
        I3_2 = I3_2.replace("_Number", str(worldmap.number))

        I3_3 = worldmap.I3_3
        I3_3 = I3_3.replace("B1NAME", b1)
        I3_3 = I3_3.replace("B2NAME", b2)
        I3_3 = I3_3.replace("B3NAME", b3)
        I3_3 = I3_3.replace("_xCoord_", str(x + 183))
        I3_3 = I3_3.replace("_yCoord_", str(y - draw.AnaloglarArasiY))
        worldmap.number += 1
        I3_3 = I3_3.replace("_Number", str(worldmap.number))

        I3 = I3_1 + worldmap.bosluk + I3_2 + worldmap.bosluk + I3_3

        In_1 = worldmap.In_1
        In_1 = In_1.replace("B1NAME", b1)
        In_1 = In_1.replace("B2NAME", b2)
        In_1 = In_1.replace("B3NAME", b3)
        In_1 = In_1.replace("_xCoord_", str(x))
        In_1 = In_1.replace("_yCoord_", str(y))
        worldmap.number += 1
        In_1 = In_1.replace("_Number", str(worldmap.number))

        In_2 = worldmap.In_2
        In_2 = In_2.replace("B1NAME", b1)
        In_2 = In_2.replace("B2NAME", b2)
        In_2 = In_2.replace("B3NAME", b3)
        In_2 = In_2.replace("_xCoord_", str(x + 24))
        In_2 = In_2.replace("_yCoord_", str(y))
        worldmap.number += 1
        In_2 = In_2.replace("_Number", str(worldmap.number))

        In_3 = worldmap.In_3
        In_3 = In_3.replace("B1NAME", b1)
        In_3 = In_3.replace("B2NAME", b2)
        In_3 = In_3.replace("B3NAME", b3)
        In_3 = In_3.replace("_xCoord_", str(x + 183))
        In_3 = In_3.replace("_yCoord_", str(y))
        worldmap.number += 1
        In_3 = In_3.replace("_Number", str(worldmap.number))

        In = In_1 + worldmap.bosluk + In_2 + worldmap.bosluk + In_3

        I = I1 + worldmap.bosluk + I2 + worldmap.bosluk + I3 + worldmap.bosluk + In
        return I
    def powerMaker(b1, b2, b3, x, y):
        P_1 = worldmap.P_1
        P_1 = P_1.replace("B1NAME", b1)
        P_1 = P_1.replace("B2NAME", b2)
        P_1 = P_1.replace("B3NAME", b3)
        P_1 = P_1.replace("_xCoord_", str(x))
        P_1 = P_1.replace("_yCoord_", str(y - draw.AnaloglarArasiY*2))
        worldmap.number += 1
        P_1 = P_1.replace("_Number", str(worldmap.number))

        P_2 = worldmap.P_2
        P_2 = P_2.replace("B1NAME", b1)
        P_2 = P_2.replace("B2NAME", b2)
        P_2 = P_2.replace("B3NAME", b3)
        P_2 = P_2.replace("_xCoord_", str(x + 24))
        P_2 = P_2.replace("_yCoord_", str(y - draw.AnaloglarArasiY*2))
        worldmap.number += 1
        P_2 = P_2.replace("_Number", str(worldmap.number))

        P_3 = worldmap.P_3
        P_3 = P_3.replace("B1NAME", b1)
        P_3 = P_3.replace("B2NAME", b2)
        P_3 = P_3.replace("B3NAME", b3)
        P_3 = P_3.replace("_xCoord_", str(x + 183))
        P_3 = P_3.replace("_yCoord_", str(y - draw.AnaloglarArasiY*2))
        worldmap.number += 1
        P_3 = P_3.replace("_Number", str(worldmap.number))

        P = P_1 + worldmap.bosluk + P_2 + worldmap.bosluk + P_3

        Q_1 = worldmap.Q_1
        Q_1 = Q_1.replace("B1NAME", b1)
        Q_1 = Q_1.replace("B2NAME", b2)
        Q_1 = Q_1.replace("B3NAME", b3)
        Q_1 = Q_1.replace("_xCoord_", str(x))
        Q_1 = Q_1.replace("_yCoord_", str(y  - draw.AnaloglarArasiY))
        worldmap.number += 1
        Q_1 = Q_1.replace("_Number", str(worldmap.number))

        Q_2 = worldmap.Q_2
        Q_2 = Q_2.replace("B1NAME", b1)
        Q_2 = Q_2.replace("B2NAME", b2)
        Q_2 = Q_2.replace("B3NAME", b3)
        Q_2 = Q_2.replace("_xCoord_", str(x + 24))
        Q_2 = Q_2.replace("_yCoord_", str(y - draw.AnaloglarArasiY))
        worldmap.number += 1
        Q_2 = Q_2.replace("_Number", str(worldmap.number))

        Q_3 = worldmap.Q_3
        Q_3 = Q_3.replace("B1NAME", b1)
        Q_3 = Q_3.replace("B2NAME", b2)
        Q_3 = Q_3.replace("B3NAME", b3)
        Q_3 = Q_3.replace("_xCoord_", str(x + 183))
        Q_3 = Q_3.replace("_yCoord_", str(y - draw.AnaloglarArasiY))
        worldmap.number += 1
        Q_3 = Q_3.replace("_Number", str(worldmap.number))

        Q = Q_1 + worldmap.bosluk + Q_2 + worldmap.bosluk + Q_3

        S_1 = worldmap.S_1
        S_1 = S_1.replace("B1NAME", b1)
        S_1 = S_1.replace("B2NAME", b2)
        S_1 = S_1.replace("B3NAME", b3)
        S_1 = S_1.replace("_xCoord_", str(x))
        S_1 = S_1.replace("_yCoord_", str(y))
        worldmap.number += 1
        S_1 = S_1.replace("_Number", str(worldmap.number))

        S_2 = worldmap.S_2
        S_2 = S_2.replace("B1NAME", b1)
        S_2 = S_2.replace("B2NAME", b2)
        S_2 = S_2.replace("B3NAME", b3)
        S_2 = S_2.replace("_xCoord_", str(x + 24))
        S_2 = S_2.replace("_yCoord_", str(y))
        worldmap.number += 1
        S_2 = S_2.replace("_Number", str(worldmap.number))

        S_3 = worldmap.S_3
        S_3 = S_3.replace("B1NAME", b1)
        S_3 = S_3.replace("B2NAME", b2)
        S_3 = S_3.replace("B3NAME", b3)
        S_3 = S_3.replace("_xCoord_", str(x + 183))
        S_3 = S_3.replace("_yCoord_", str(y))
        worldmap.number += 1
        S_3 = S_3.replace("_Number", str(worldmap.number))

        S = S_1 + worldmap.bosluk + S_2 + worldmap.bosluk + S_3
        power = P + worldmap.bosluk + Q + worldmap.bosluk + S + worldmap.bosluk + draw.whiteBox(x - 15, y - draw.AnaloglarArasiY*3)
        return power
    def F_Imaker(b1, b2, b3, x, y):
        F_I1_1 = worldmap.F_I1_1
        F_I1_1 = F_I1_1.replace("B1NAME", b1)
        F_I1_1 = F_I1_1.replace("B2NAME", b2)
        F_I1_1 = F_I1_1.replace("B3NAME", b3)
        F_I1_1 = F_I1_1.replace("_xCoord_", str(x))
        F_I1_1 = F_I1_1.replace("_yCoord_", str(y - 72 - 72))
        worldmap.number += 1
        F_I1_1 = F_I1_1.replace("_Number", str(worldmap.number))

        F_I1_2 = worldmap.F_I1_2
        F_I1_2 = F_I1_2.replace("B1NAME", b1)
        F_I1_2 = F_I1_2.replace("B2NAME", b2)
        F_I1_2 = F_I1_2.replace("B3NAME", b3)
        F_I1_2 = F_I1_2.replace("_xCoord_", str(x + 24))
        F_I1_2 = F_I1_2.replace("_yCoord_", str(y - 72 - 72))
        worldmap.number += 1
        F_I1_2 = F_I1_2.replace("_Number", str(worldmap.number))

        F_I1_3 = worldmap.F_I1_3
        F_I1_3 = F_I1_3.replace("B1NAME", b1)
        F_I1_3 = F_I1_3.replace("B2NAME", b2)
        F_I1_3 = F_I1_3.replace("B3NAME", b3)
        F_I1_3 = F_I1_3.replace("_xCoord_", str(x + 183))
        F_I1_3 = F_I1_3.replace("_yCoord_", str(y - 72 - 72 + 73))
        worldmap.number += 1
        F_I1_3 = F_I1_3.replace("_Number", str(worldmap.number))

        F_I1 = F_I1_1 + worldmap.bosluk + F_I1_2 + worldmap.bosluk + F_I1_3

        F_I2_1 = worldmap.F_I2_1
        F_I2_1 = F_I2_1.replace("B1NAME", b1)
        F_I2_1 = F_I2_1.replace("B2NAME", b2)
        F_I2_1 = F_I2_1.replace("B3NAME", b3)
        F_I2_1 = F_I2_1.replace("_xCoord_", str(x))
        F_I2_1 = F_I2_1.replace("_yCoord_", str(y - 72))
        worldmap.number += 1
        F_I2_1 = F_I2_1.replace("_Number", str(worldmap.number))

        F_I2_2 = worldmap.F_I2_2
        F_I2_2 = F_I2_2.replace("B1NAME", b1)
        F_I2_2 = F_I2_2.replace("B2NAME", b2)
        F_I2_2 = F_I2_2.replace("B3NAME", b3)
        F_I2_2 = F_I2_2.replace("_xCoord_", str(x + 24))
        F_I2_2 = F_I2_2.replace("_yCoord_", str(y - 72))
        worldmap.number += 1
        F_I2_2 = F_I2_2.replace("_Number", str(worldmap.number))

        F_I2_3 = worldmap.F_I2_3
        F_I2_3 = F_I2_3.replace("B1NAME", b1)
        F_I2_3 = F_I2_3.replace("B2NAME", b2)
        F_I2_3 = F_I2_3.replace("B3NAME", b3)
        F_I2_3 = F_I2_3.replace("_xCoord_", str(x + 183))
        F_I2_3 = F_I2_3.replace("_yCoord_", str(y - 72 + 73))
        worldmap.number += 1
        F_I2_3 = F_I2_3.replace("_Number", str(worldmap.number))

        F_I2 = F_I2_1 + worldmap.bosluk + F_I2_2 + worldmap.bosluk + F_I2_3

        F_I3_1 = worldmap.F_I3_1
        F_I3_1 = F_I3_1.replace("B1NAME", b1)
        F_I3_1 = F_I3_1.replace("B2NAME", b2)
        F_I3_1 = F_I3_1.replace("B3NAME", b3)
        F_I3_1 = F_I3_1.replace("_xCoord_", str(x))
        F_I3_1 = F_I3_1.replace("_yCoord_", str(y))
        worldmap.number += 1
        F_I3_1 = F_I3_1.replace("_Number", str(worldmap.number))

        F_I3_2 = worldmap.F_I3_2
        F_I3_2 = F_I3_2.replace("B1NAME", b1)
        F_I3_2 = F_I3_2.replace("B2NAME", b2)
        F_I3_2 = F_I3_2.replace("B3NAME", b3)
        F_I3_2 = F_I3_2.replace("_xCoord_", str(x + 24))
        F_I3_2 = F_I3_2.replace("_yCoord_", str(y))
        worldmap.number += 1
        F_I3_2 = F_I3_2.replace("_Number", str(worldmap.number))

        F_I3_3 = worldmap.F_I3_3
        F_I3_3 = F_I3_3.replace("B1NAME", b1)
        F_I3_3 = F_I3_3.replace("B2NAME", b2)
        F_I3_3 = F_I3_3.replace("B3NAME", b3)
        F_I3_3 = F_I3_3.replace("_xCoord_", str(x + 183))
        F_I3_3 = F_I3_3.replace("_yCoord_", str(y + 73))
        worldmap.number += 1
        F_I3_3 = F_I3_3.replace("_Number", str(worldmap.number))

        F_I3 = F_I3_1 + worldmap.bosluk + F_I3_2 + worldmap.bosluk + F_I3_3

        F_I = F_I1 + worldmap.bosluk + F_I2 + worldmap.bosluk + F_I3

        return F_I
    def resetMaker(b1, b2, b3, x, y):
        reset = worldmap.reset
        reset = reset.replace("B1NAME", b1)
        reset = reset.replace("B2NAME", b2)
        reset = reset.replace("B3NAME", b3)
        reset = reset.replace("_xCoord_", str(x))
        reset = reset.replace("_yCoord_", str(y))
        worldmap.number += 1
        reset = reset.replace("_Number", str(worldmap.number))
        return reset
    def senaryoMaker(b1, b2, b3, x, y):
        butonWidth = int(40)
        butonHeight = int(37)
        AradakiBosluk = int(10)
        senA = worldmap.senA
        senA = senA.replace("B1NAME", b1)
        senA = senA.replace("B2NAME", b2)
        senA = senA.replace("B3NAME", b3)
        senA = senA.replace("_xCoord_", str(x))
        senA = senA.replace("_yCoord_", str(y))
        worldmap.number += 1
        senA = senA.replace("_Number", str(worldmap.number))

        senB = worldmap.senB
        senB = senB.replace("B1NAME", b1)
        senB = senB.replace("B2NAME", b2)
        senB = senB.replace("B3NAME", b3)
        senB = senB.replace("_xCoord_", str(x+ butonWidth + AradakiBosluk))
        senB = senB.replace("_yCoord_", str(y))
        worldmap.number += 1
        senB = senB.replace("_Number", str(worldmap.number))

        senC = worldmap.senC
        senC = senC.replace("B1NAME", b1)
        senC = senC.replace("B2NAME", b2)
        senC = senC.replace("B3NAME", b3)
        senC = senC.replace("_xCoord_", str(x + butonWidth + AradakiBosluk+ butonWidth + AradakiBosluk))
        senC = senC.replace("_yCoord_", str(y))
        worldmap.number += 1
        senC = senC.replace("_Number", str(worldmap.number))

        senD = worldmap.senD
        senD = senD.replace("B1NAME", b1)
        senD = senD.replace("B2NAME", b2)
        senD = senD.replace("B3NAME", b3)
        senD = senD.replace("_xCoord_", str(x + butonWidth + AradakiBosluk+ butonWidth + AradakiBosluk+ butonWidth + AradakiBosluk))
        senD = senD.replace("_yCoord_", str(y))
        worldmap.number += 1
        senD = senD.replace("_Number", str(worldmap.number))

        senaryo = senA + worldmap.bosluk + senB + worldmap.bosluk + senC + worldmap.bosluk + senD
        return senaryo
    def FeedNameMaker(b1, b2, b3, x, y):
        x = x + 40 - int((len(b3)+1)/2)*10
        FeedName = worldmap.FeedName
        FeedName = FeedName.replace("B1NAME", b1)
        FeedName = FeedName.replace("B2NAME", b2)
        FeedName = FeedName.replace("B3NAME", b3)
        FeedName = FeedName.replace("_xCoord_", str(x))
        FeedName = FeedName.replace("_yCoord_", str(y))
        worldmap.number += 1
        FeedName = FeedName.replace("_Number", str(worldmap.number))

        FeedNameS = worldmap.FeedNameS
        FeedNameS = FeedNameS.replace("B1NAME", b1)
        FeedNameS = FeedNameS.replace("B2NAME", b2)
        FeedNameS = FeedNameS.replace("B3NAME", b3)
        FeedNameS = FeedNameS.replace("_xCoord_", str(x - 150))
        FeedNameS = FeedNameS.replace("_yCoord_", str(y))
        worldmap.number += 1
        FeedNameS = FeedNameS.replace("_Number", str(worldmap.number))

        return FeedName + worldmap.bosluk + FeedNameS
    def LBSmaker(b1, b2, b3, x, y):
        LBS = worldmap.LBS
        LBS = LBS.replace("B1NAME", b1)
        LBS = LBS.replace("B2NAME", b2)
        LBS = LBS.replace("B3NAME", b3)
        LBS = LBS.replace("_xCoord_", str(x))
        LBS = LBS.replace("_yCoord_", str(y))
        worldmap.number += 1
        LBS = LBS.replace("_Number", str(worldmap.number))

        LBS_S = worldmap.LBS_S
        LBS_S = LBS_S.replace("B1NAME", b1)
        LBS_S = LBS_S.replace("B2NAME", b2)
        LBS_S = LBS_S.replace("B3NAME", b3)
        LBS_S = LBS_S.replace("_xCoord_", str(x - 150))
        LBS_S = LBS_S.replace("_yCoord_", str(y + 10))
        worldmap.number += 1
        LBS_S = LBS_S.replace("_Number", str(worldmap.number))

        return LBS + worldmap.bosluk + LBS_S
    def topoShort(b1, b2, b3, x, y):
        topo1Short = worldmap.topo1Short
        topo1Short = topo1Short.replace("B1NAME", b1)
        topo1Short = topo1Short.replace("B2NAME", b2)
        topo1Short = topo1Short.replace("B3NAME", b3)
        topo1Short = topo1Short.replace("_xCoord_", str(x))
        topo1Short = topo1Short.replace("_yCoord_", str(y))
        worldmap.number += 1
        topo1Short = topo1Short.replace("_Number", str(worldmap.number))

        topo1Short_ = worldmap.topo1Short_
        topo1Short_ = topo1Short_.replace("B1NAME", b1)
        topo1Short_ = topo1Short_.replace("B2NAME", b2)
        topo1Short_ = topo1Short_.replace("B3NAME", b3)
        topo1Short_ = topo1Short_.replace("_xCoord_", str(x))
        topo1Short_ = topo1Short_.replace("_yCoord_", str(y + 139))
        worldmap.number += 1
        topo1Short_ = topo1Short_.replace("_Number", str(worldmap.number))

        return topo1Short + worldmap.bosluk + topo1Short_
    def isoLine(b1, b2, b3, x, y):
        isoLine = worldmap.isoLine
        isoLine = isoLine.replace("B1NAME", b1)
        isoLine = isoLine.replace("B2NAME", b2)
        isoLine = isoLine.replace("B3NAME", b3)
        isoLine = isoLine.replace("_xCoord_", str(x))
        isoLine = isoLine.replace("_yCoord_", str(y))
        worldmap.number += 1
        isoLine = isoLine.replace("_Number", str(worldmap.number))

        isoLineS = worldmap.isoLineS
        isoLineS = isoLineS.replace("B1NAME", b1)
        isoLineS = isoLineS.replace("B2NAME", b2)
        isoLineS = isoLineS.replace("B3NAME", b3)
        isoLineS = isoLineS.replace("_xCoord_", str(x - 150))
        isoLineS = isoLineS.replace("_yCoord_", str(y + 10))
        worldmap.number += 1
        isoLineS = isoLineS.replace("_Number", str(worldmap.number))

        return isoLine + worldmap.bosluk + isoLineS
    def topo2_2Maker(b1, b2, b3, x, y):
        topo2_2 = worldmap.topo2_2
        topo2_2 = topo2_2.replace("B1NAME", b1)
        topo2_2 = topo2_2.replace("B2NAME", b2)
        topo2_2 = topo2_2.replace("B3NAME", b3)
        topo2_2 = topo2_2.replace("_xCoord_", str(x))
        topo2_2 = topo2_2.replace("_yCoord_", str(y))
        worldmap.number += 1
        topo2_2 = topo2_2.replace("_Number", str(worldmap.number))
        return topo2_2
    def topo3(b1, b2, b3, x, y):
        topo3 = worldmap.topo3
        topo3 = topo3.replace("B1NAME", b1)
        topo3 = topo3.replace("B2NAME", b2)
        topo3 = topo3.replace("B3NAME", b3)
        topo3 = topo3.replace("_xCoord_", str(x))
        topo3 = topo3.replace("_yCoord_", str(y))
        worldmap.number += 1
        topo3 = topo3.replace("_Number", str(worldmap.number))
        return topo3
    def gerilimOlcuMaker(b1, b2, b3, x, y):
        worldmap.number += 1
        grupNo = worldmap.number
        gerilimOlcu1 = worldmap.gerilimOlcu1
        gerilimOlcu1 = gerilimOlcu1.replace("B1NAME", b1)
        gerilimOlcu1 = gerilimOlcu1.replace("B2NAME", b2)
        gerilimOlcu1 = gerilimOlcu1.replace("B3NAME", b3)
        gerilimOlcu1 = gerilimOlcu1.replace("_xCoord_", str(x))
        gerilimOlcu1 = gerilimOlcu1.replace("_yCoord_", str(y))
        worldmap.number += 1
        gerilimOlcu1 = gerilimOlcu1.replace("_Number", str(worldmap.number))
        gerilimOlcu1 = gerilimOlcu1.replace("_Number1", str(grupNo))

        gerilimOlcu2 = worldmap.gerilimOlcu2
        gerilimOlcu2 = gerilimOlcu2.replace("B1NAME", b1)
        gerilimOlcu2 = gerilimOlcu2.replace("B2NAME", b2)
        gerilimOlcu2 = gerilimOlcu2.replace("B3NAME", b3)
        gerilimOlcu2 = gerilimOlcu2.replace("_xCoord_", str(x + 28))
        gerilimOlcu2 = gerilimOlcu2.replace("_yCoord_", str(y + 35))
        worldmap.number += 1
        gerilimOlcu2 = gerilimOlcu2.replace("_Number", str(worldmap.number))
        gerilimOlcu2 = gerilimOlcu2.replace("_Number1", str(grupNo))

        gerilimOlcu3 = worldmap.gerilimOlcu3
        gerilimOlcu3 = gerilimOlcu3.replace("B1NAME", b1)
        gerilimOlcu3 = gerilimOlcu3.replace("B2NAME", b2)
        gerilimOlcu3 = gerilimOlcu3.replace("B3NAME", b3)
        gerilimOlcu3 = gerilimOlcu3.replace("_xCoord_", str(x - 28))
        gerilimOlcu3 = gerilimOlcu3.replace("_yCoord_", str(y + 35))
        worldmap.number += 1
        gerilimOlcu3 = gerilimOlcu3.replace("_Number", str(worldmap.number))
        gerilimOlcu3 = gerilimOlcu3.replace("_Number1", str(grupNo))
        return gerilimOlcu1 + worldmap.bosluk + gerilimOlcu2 + worldmap.bosluk + gerilimOlcu3
    def kesiciAyiriciToprak(B1, B2, B3, x, y, b3dict):
        isoBB = draw.isoBBmaker(B1, B2, B3, x + 200 - 41, y + 65)
        station =  isoBB
        topo1 = draw.topoMaker(B1, B2, B3, x + 200, y + 65 + 82)
        station = station + worldmap.bosluk + topo1
        cb = draw.cbMaker(B1, B2, B3, x + 200 - 41, y + 65 + 82 + 114)
        station = station + worldmap.bosluk + cb
        topo2_1 = draw.topo2_1Maker(B1, B2, B3, x + 200, y + 65 + 82 + 114 + 82)
        station = station + worldmap.bosluk + topo2_1
        isoEth = draw.isoEthMaker(B1, B2, B3, x + 200 + 64, y + 65 + 82 + 114 + 82 + 140 - 20)
        station = station + worldmap.bosluk + isoEth
        FeedName = draw.FeedNameMaker(B1, B2, B3, x + 200 - 54, y - 100)
        station = station + worldmap.bosluk + FeedName
        if("I1" in b3dict and "I2" in b3dict):
            I = draw.Imaker(B1, B2, B3, x + 200 - 135, y - draw.AnalogStartingY)
            station = station + worldmap.bosluk + I
        if ("P" in b3dict and "Q" in b3dict):
            power = draw.powerMaker(B1, B2, B3, x + 200 - 135, y - draw.PowerStartingY)
            station = station + worldmap.bosluk + power
        if("F-I1" in b3dict):
            F_I = draw.F_Imaker(B1, B2, B3, x + 200 - 135, y - draw.F_IStartingY)
            station = station + worldmap.bosluk + F_I
        if("Reset" in b3dict):
            reset = draw.resetMaker(B1, B2, B3, x + 200 - int(draw.resetWidth/2), y - draw.ResetStartingY)
            station = station + worldmap.bosluk + reset
        if("SetGrp" in b3dict):
            senaryo = draw.senaryoMaker(B1, B2, B3, x + 200 - int(draw.resetWidth/2), y - draw.ResetStartingY - draw.resetHeight - 10)
            station = station + worldmap.bosluk + senaryo
        if ("cosPhi" in b3dict):
            cosPhi = draw.cosPhiMaker(B1, B2, B3, x + 200 - 135, y - draw.CosPhiStartingY)
            station = station + worldmap.bosluk + cosPhi

        return station
    def kesiciAyiriciHat(B1, B2, B3, x, y, b3dict):
        isoBB = draw.isoBBmaker(B1, B2, B3, x + 200 - 41, y + 65)
        station =  isoBB
        topo1 = draw.topoMaker(B1, B2, B3, x + 200, y + 65 + 82)
        station = station + worldmap.bosluk + topo1
        cb = draw.cbMaker(B1, B2, B3, x + 200 - 41, y + 65 + 82 + 114)
        station = station + worldmap.bosluk + cb
        topo2_2 = draw.topo2_2Maker(B1, B2, B3, x + 200, y + 65 + 82 + 114 + 82)
        station = station + worldmap.bosluk + topo2_2
        isoLine = draw.isoLine(B1, B2, B3, x + 200 - 41, y + 65 + 82 + 114 + 82 + 114)
        station = station + worldmap.bosluk + isoLine
        topo3 = draw.topo3(B1, B2, B3, x + 200, y + 65 + 82 + 114 + 82 + 114 + 82)
        station = station + worldmap.bosluk + topo3
        FeedName = draw.FeedNameMaker(B1, B2, B3, x + 200 - 54, y - 100)
        station = station + worldmap.bosluk + FeedName
        if ("I1" in b3dict and "I2" in b3dict):
            I = draw.Imaker(B1, B2, B3, x + 200 - 135, y - draw.AnalogStartingY)
            station = station + worldmap.bosluk + I
        if ("P" in b3dict and "Q" in b3dict):
            power = draw.powerMaker(B1, B2, B3, x + 200 - 135, y - draw.PowerStartingY)
            station = station + worldmap.bosluk + power
        if ("F-I1" in b3dict):
            F_I = draw.F_Imaker(B1, B2, B3, x + 200 - 135, y - draw.F_IStartingY)
            station = station + worldmap.bosluk + F_I
        if ("Reset" in b3dict):
            reset = draw.resetMaker(B1, B2, B3, x + 200 - int(draw.resetWidth / 2), y - draw.ResetStartingY)
            station = station + worldmap.bosluk + reset
        if ("SetGrp" in b3dict):
            senaryo = draw.senaryoMaker(B1, B2, B3, x + 200 - int(draw.resetWidth / 2),
                                        y - draw.ResetStartingY - draw.resetHeight - 10)
            station = station + worldmap.bosluk + senaryo
        if ("cosPhi" in b3dict):
            cosPhi = draw.cosPhiMaker(B1, B2, B3, x + 200 - 135, y - draw.CosPhiStartingY)
            station = station + worldmap.bosluk + cosPhi

        return station
    def baraToprak(B1, B2, B3, x, y, b3dict):
        isoBB = draw.isoBBmaker(B1, B2, B3, x + 200 - 41, y + 65)
        station = isoBB
        topo1 = draw.topoShort(B1, B2, B3, x + 200 , y + 65 + 82)
        station = station + worldmap.bosluk + topo1
        isoEth = draw.isoEthMaker(B1, B2, B3, x + 200 + 64, y + 65 + 82 + 139 - 20)
        station = station + worldmap.bosluk + isoEth
        FeedName = draw.FeedNameMaker(B1, B2, B3, x + 200 - 54, y - 100)
        station = station + worldmap.bosluk + FeedName
        if ("I1" in b3dict and "I2" in b3dict):
            I = draw.Imaker(B1, B2, B3, x + 200 - 135, y - 618)
            station = station + worldmap.bosluk + I
        if ("P" in b3dict and "Q" in b3dict):
            power = draw.powerMaker(B1, B2, B3, x + 200 - 135, y - 618 + 48)
            station = station + worldmap.bosluk + power
        if ("F-I1" in b3dict):
            F_I = draw.F_Imaker(B1, B2, B3, x + 200 - 135, y - 618 - 260)
            station = station + worldmap.bosluk + F_I
        if ("Reset" in b3dict):
            reset = draw.resetMaker(B1, B2, B3, x + 200 - 200, y - 618 - 96)
            station = station + worldmap.bosluk + reset
        if ("SetGrp" in b3dict):
            senaryo = draw.senaryoMaker(B1, B2, B3, x + 200 - 200, y - 618 - 98 - 76)
            station = station + worldmap.bosluk + senaryo
        return  station
    def bara(B1, B2, B3, x, y, b3dict):
        isoBB = draw.isoBBmaker(B1, B2, B3, x + 200 - 41, y + 65)
        station = isoBB
        topo1 = draw.topoMaker(B1, B2, B3, x + 200, y + 65 + 82)
        station = station + worldmap.bosluk + topo1
        FeedName = draw.FeedNameMaker(B1, B2, B3, x + 200 - 54, y - 100)
        station = station + worldmap.bosluk + FeedName
        if ("I1" in b3dict and "I2" in b3dict):
            I = draw.Imaker(B1, B2, B3, x + 200 - 135, y - 618)
            station = station + worldmap.bosluk + I
        if ("P" in b3dict and "Q" in b3dict):
            power = draw.powerMaker(B1, B2, B3, x + 200 - 135, y - 618 + 48)
            station = station + worldmap.bosluk + power
        if ("F-I1" in b3dict):
            F_I = draw.F_Imaker(B1, B2, B3, x + 200 - 135, y - 618 - 260)
            station = station + worldmap.bosluk + F_I
        if ("Reset" in b3dict):
            reset = draw.resetMaker(B1, B2, B3, x + 200 - 200, y - 618 - 96)
            station = station + worldmap.bosluk + reset
        if ("SetGrp" in b3dict):
            senaryo = draw.senaryoMaker(B1, B2, B3, x + 200 - 200, y - 618 - 98 - 76)
            station = station + worldmap.bosluk + senaryo
        return station
    def kesiciAyirici(B1, B2, B3, x, y, b3dict):
        isoBB = draw.isoBBmaker(B1, B2, B3, x + 200 - 41, y + 65)
        station =  isoBB
        topo1 = draw.topoMaker(B1, B2, B3, x + 200, y + 65 + 82)
        station = station + worldmap.bosluk + topo1
        cb = draw.cbMaker(B1, B2, B3, x + 200 - 41, y + 65 + 82 + 114)
        station = station + worldmap.bosluk + cb
        topo2_2 = draw.topo2_2Maker(B1, B2, B3, x + 200, y + 65 + 82 + 114 + 82)
        station = station + worldmap.bosluk + topo2_2
        FeedName = draw.FeedNameMaker(B1, B2, B3, x + 200 - 54, y - 100)
        station = station + worldmap.bosluk + FeedName
        if ("I1" in b3dict and "I2" in b3dict):
            I = draw.Imaker(B1, B2, B3, x + 200 - 135, y - draw.AnalogStartingY)
            station = station + worldmap.bosluk + I
        if ("P" in b3dict and "Q" in b3dict):
            power = draw.powerMaker(B1, B2, B3, x + 200 - 135, y - draw.PowerStartingY)
            station = station + worldmap.bosluk + power
        if ("F-I1" in b3dict):
            F_I = draw.F_Imaker(B1, B2, B3, x + 200 - 135, y - draw.F_IStartingY)
            station = station + worldmap.bosluk + F_I
        if ("Reset" in b3dict):
            reset = draw.resetMaker(B1, B2, B3, x + 200 - int(draw.resetWidth / 2), y - draw.ResetStartingY)
            station = station + worldmap.bosluk + reset
        if ("SetGrp" in b3dict):
            senaryo = draw.senaryoMaker(B1, B2, B3, x + 200 - int(draw.resetWidth / 2),
                                        y - draw.ResetStartingY - draw.resetHeight - 10)
            station = station + worldmap.bosluk + senaryo
        if ("cosPhi" in b3dict):
            cosPhi = draw.cosPhiMaker(B1, B2, B3, x + 200 - 135, y - draw.CosPhiStartingY)
            station = station + worldmap.bosluk + cosPhi
        return station
    def LBSToprak(B1, B2, B3, x, y, b3dict):
        LBS = draw.LBSmaker(B1, B2, B3, x + 200 - 41, y + 65)
        station = LBS
        topo1 = draw.topoShort(B1, B2, B3, x + 200 , y + 65 + 82)
        station = station + worldmap.bosluk + topo1
        isoEth = draw.isoEthMaker(B1, B2, B3, x + 200 + 64, y + 65 + 82 + 139 - 20)
        station = station + worldmap.bosluk + isoEth
        FeedName = draw.FeedNameMaker(B1, B2, B3, x + 200 - 54, y - 100)
        station = station + worldmap.bosluk + FeedName
        if ("I1" in b3dict and "I2" in b3dict):
            I = draw.Imaker(B1, B2, B3, x + 200 - 135, y - draw.AnalogStartingY)
            station = station + worldmap.bosluk + I
        if ("P" in b3dict and "Q" in b3dict):
            power = draw.powerMaker(B1, B2, B3, x + 200 - 135, y - draw.PowerStartingY)
            station = station + worldmap.bosluk + power
        if ("F-I1" in b3dict):
            F_I = draw.F_Imaker(B1, B2, B3, x + 200 - 135, y - draw.F_IStartingY)
            station = station + worldmap.bosluk + F_I
        if ("Reset" in b3dict):
            reset = draw.resetMaker(B1, B2, B3, x + 200 - int(draw.resetWidth / 2), y - draw.ResetStartingY)
            station = station + worldmap.bosluk + reset
        if ("SetGrp" in b3dict):
            senaryo = draw.senaryoMaker(B1, B2, B3, x + 200 - int(draw.resetWidth / 2),
                                        y - draw.ResetStartingY - draw.resetHeight - 10)
            station = station + worldmap.bosluk + senaryo
        if ("cosPhi" in b3dict):
            cosPhi = draw.cosPhiMaker(B1, B2, B3, x + 200 - 135, y - draw.CosPhiStartingY)
            station = station + worldmap.bosluk + cosPhi
        return  station
    def threeDmaker(x, y, width, height):
        sol = worldmap.dikey.replace("_Height_", str(height))
        sol = sol.replace("_xCoord_", str(x))
        sol = sol.replace("_yCoord_", str(y))
        sol = sol.replace("_renk_", str(worldmap.beyazRenk))
        worldmap.number += 1
        sol = sol.replace("_Number", str(worldmap.number))
        ust = worldmap.yatay.replace("_Width_", str(width))
        ust = ust.replace("_xCoord_", str(x))
        ust = ust.replace("_yCoord_", str(y))
        ust = ust.replace("_renk_", str(worldmap.beyazRenk))
        worldmap.number += 1
        ust = ust.replace("_Number", str(worldmap.number))
        sag = worldmap.dikey.replace("_Height_", str(height))
        sag = sag.replace("_xCoord_", str(x + width))
        sag = sag.replace("_yCoord_", str(y))
        sag = sag.replace("_renk_", str(worldmap.siyahRenk))
        worldmap.number += 1
        sag = sag.replace("_Number", str(worldmap.number))
        alt = worldmap.yatay.replace("_Width_", str(width))
        alt = alt.replace("_xCoord_", str(x))
        alt = alt.replace("_yCoord_", str(y + height))
        alt = alt.replace("_renk_", str(worldmap.siyahRenk))
        worldmap.number += 1
        alt = alt.replace("_Number", str(worldmap.number))
        return sol + worldmap.bosluk + ust + worldmap.bosluk + sag + worldmap.bosluk + alt
    def butonB1Maker(x, y, b1name):
        width = 1344
        height = 169
        text = worldmap.textB1
        text = text.replace("B1NAME", b1name)
        text = text.replace("_xCoord_", str(x + int(width/2) - 290))
        text = text.replace("_yCoord_", str(y + int(height/2) - 63))
        worldmap.number += 1
        text = text.replace("_Number", str(worldmap.number))
        buton = worldmap.buton
        buton = buton.replace("_xCoord_", str(x))
        buton = buton.replace("_yCoord_", str(y))
        buton = buton.replace("B1NAME", b1name)
        worldmap.number += 1
        buton = buton.replace("_Number", str(worldmap.number))
        ucboyut = draw.threeDmaker(x, y, width, height)
        buton = buton + worldmap.bosluk + ucboyut + worldmap.bosluk +text

        return buton
    def whiteBox (x, y):
        width = 1344
        height = 169
        wBox = worldmap.whiteBox
        wBox = wBox.replace("_xCoord_", str(x ))
        wBox = wBox.replace("_yCoord_", str(y ))
        worldmap.number += 1
        wBox = wBox.replace("_Number", str(worldmap.number))
        return wBox
    def analogText(x,y):
        analogTextIn = worldmap.analogText.replace("ANALOGTEXT", "In")
        analogTextIn = analogTextIn.replace("_xCoord_", str(x))
        analogTextIn = analogTextIn.replace("_yCoord_", str(y - draw.AnalogStartingY))
        worldmap.number += 1
        analogTextIn = analogTextIn.replace("_Number", str(worldmap.number))

        analogTextI3 = worldmap.analogText.replace("ANALOGTEXT", "I3")
        analogTextI3 = analogTextI3.replace("_xCoord_", str(x))
        analogTextI3 = analogTextI3.replace("_yCoord_", str(y - draw.AnalogStartingY - draw.AnaloglarArasiY))
        worldmap.number += 1
        analogTextI3 = analogTextI3.replace("_Number", str(worldmap.number))

        analogTextI2 = worldmap.analogText.replace("ANALOGTEXT", "I2")
        analogTextI2 = analogTextI2.replace("_xCoord_", str(x))
        analogTextI2 = analogTextI2.replace("_yCoord_", str(y - draw.AnalogStartingY - draw.AnaloglarArasiY*2))
        worldmap.number += 1
        analogTextI2 = analogTextI2.replace("_Number", str(worldmap.number))

        analogTextI1 = worldmap.analogText.replace("ANALOGTEXT", "I1")
        analogTextI1 = analogTextI1.replace("_xCoord_", str(x))
        analogTextI1 = analogTextI1.replace("_yCoord_", str(y - draw.AnalogStartingY - draw.AnaloglarArasiY * 3))
        worldmap.number += 1
        analogTextI1 = analogTextI1.replace("_Number", str(worldmap.number))

        analogTextCosPhi = worldmap.analogText.replace("ANALOGTEXT", "CosPhi")
        analogTextCosPhi = analogTextCosPhi.replace("_xCoord_", str(x))
        analogTextCosPhi = analogTextCosPhi.replace("_yCoord_", str(y - draw.AnalogStartingY - draw.AnaloglarArasiY * 4))
        worldmap.number += 1
        analogTextCosPhi = analogTextCosPhi.replace("_Number", str(worldmap.number))

        analogTextS = worldmap.analogText.replace("ANALOGTEXT", "S")
        analogTextS = analogTextS.replace("_xCoord_", str(x))
        analogTextS = analogTextS.replace("_yCoord_",str(y - draw.AnalogStartingY - draw.AnaloglarArasiY * 5))
        worldmap.number += 1
        analogTextS = analogTextS.replace("_Number", str(worldmap.number))

        analogTextQ = worldmap.analogText.replace("ANALOGTEXT", "Q")
        analogTextQ = analogTextQ.replace("_xCoord_", str(x))
        analogTextQ = analogTextQ.replace("_yCoord_", str(y - draw.AnalogStartingY - draw.AnaloglarArasiY * 6))
        worldmap.number += 1
        analogTextQ = analogTextQ.replace("_Number", str(worldmap.number))

        analogTextP = worldmap.analogText.replace("ANALOGTEXT", "P")
        analogTextP = analogTextP.replace("_xCoord_", str(x))
        analogTextP = analogTextP.replace("_yCoord_", str(y - draw.AnalogStartingY - draw.AnaloglarArasiY * 7))
        worldmap.number += 1
        analogTextP = analogTextP.replace("_Number", str(worldmap.number))

        analogtext = analogTextP + worldmap.bosluk + analogTextQ + worldmap.bosluk + analogTextS + worldmap.bosluk + analogTextCosPhi + worldmap.bosluk + analogTextI1 + worldmap.bosluk + analogTextI2 + worldmap.bosluk + analogTextI3 + worldmap.bosluk + analogTextIn

        return analogtext
    def onlyCB(B1, B2, B3, x, y, b3dict):
        cb = draw.cbMaker(B1, B2, B3, x + 200 - 41, y + 65)
        topo1 = draw.topoMaker(B1, B2, B3, x + 200, y + 65 + 82)
        FeedName = draw.FeedNameMaker(B1, B2, B3, x + 200 - 54, y - 100)
        station = cb + worldmap.bosluk + topo1 + worldmap.bosluk + FeedName
        return station






