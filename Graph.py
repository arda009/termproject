import csv
from tkinter.filedialog import askopenfilename
import queue

class GraphNode:
    def __init__(self, name, type, x, y):
        self.name = name
        self.type = type
        self.x = x
        self.y = y
        self.addressed = False #başkasının komşusu mu? barayı node olarak yaratmak için
        self.neighbors = []
        self.layer = 0
        self.xUnit = 1
        self.isdummy = False
        self.busbarsize = 0
    def add_neighbor(self, edge, node):
        self.neighbors.append({"neighbor":node,"edge":edge})

class Graph:
    def __init__(self):
        self.nodes = {}
        self.edges = {}
        self.rootName = ""

    def __contains__(self, key):
        return key in self.nodes

    def add_vertex(self, node_name, type, x=0, y=0, isdummy = False, layer = 0):
        if node_name not in self.nodes:
            new_node = GraphNode(node_name, type, x, y)
            self.nodes[node_name] = new_node
            if isdummy:
                new_node.isdummy = True
                new_node.layer = layer
            return new_node

    def add_edge(self, edge_name, from_node, to_node, from_type="UNKNOWN", to_type="UNKNOWN"):
        if from_node not in self.nodes:
            self.add_vertex(from_node, from_type)
        if to_node not in self.nodes:
            self.add_vertex(to_node, to_type)
        self.nodes[to_node].addressed = True
        self.nodes[from_node].add_neighbor(edge_name, self.nodes[to_node])
        self.edges[(from_node, to_node)] = {"name": edge_name}#edges dict içinde from_node ve to_node tupple'ı var key olarak onun da value'su "name": edge_name dictionarysi

    def get_neighbors(self, node_name):
        the_neighbors = []
        if node_name in self.nodes:
            for n in self.nodes[node_name].neighbors:
                the_neighbors.append(n["neighbor"].name)
        return the_neighbors

    def get_edge_data(self, node_name_1, node_name_2):# iki nodun arasında edge varsa edge dönüyor.
        if (node_name_1, node_name_2) in self.edges:
            return self.edges[(node_name_1, node_name_2)]
        else:
            return None

    def createParentNode(self, rootName, rootEdgeName):#busbar yaratmak için rootName:Busbar ismi sonradan set edilecek rootEdgeName:Busbar topo ismi sonradan set edilecek
        the_parent = self.add_vertex(rootName, "BB")
        for n in self.nodes:
            if not self.nodes[n].addressed:
                self.add_edge(rootEdgeName, rootName, self.nodes[n].name)


def build_graph():
    CSVname = askopenfilename()
    f = open(CSVname)
    csv_f = csv.reader(f)
    rowList = []
    for row in csv_f:
        rowList.append(row)
        print(row)
    rowList = rowList[1:len(rowList)]
    rowList.sort()
    f.close()
    pathArray = {}
    for i in range(len(rowList)):  # path'lerin altında connect olan topolar
        tmp = str(rowList[i])
        x = tmp.split(",")
        pathH = str(x[0])
        pathH = pathH[2:len(pathH)]
        connectedL = str(x[1])
        connectedL = connectedL[2:len(connectedL)-1]
        if pathH in pathArray:
            pathArray[pathH].append(connectedL)
        else:
            pathArray[pathH] = [connectedL]

    list_terminal = terminal_finder(pathArray)
    terminal_1 = list_terminal[0]
    terminal_2 = list_terminal[1]
    root=[]

    #Graph Building
    G = Graph()
    for key in pathArray:
        for key2 in pathArray:
            if pathArray.get(key) == pathArray.get(key2) and (key != key2):
                if (get_terminal(key) == terminal_2) and (get_terminal(key2) == terminal_1):
                    G.add_edge(pathArray[key][0], path_combine(key.split('/')), path_combine(key2.split('/')), typeFinder(key), typeFinder(key2))
                elif get_terminal(key) == terminal_1 and get_terminal(key2) == terminal_1 and not has_second_terminal(pathArray, key, terminal_2): #bara için t2 terminali olmayan
                    G.add_edge(pathArray[key][0], path_combine(key.split('/')), path_combine(key2.split('/')),
                               typeFinder(key), typeFinder(key2))
                    root.append(path_combine(key.split('/')))
                elif get_terminal(key) == terminal_2 and not has_first_terminal(pathArray, key2, terminal_1): #toprak için key ve key2 yer değiştirdi
                    G.add_edge(pathArray[key][0], path_combine(key.split('/')), path_combine(key2.split('/')),
                               typeFinder(key), typeFinder(key2))

    return G, root

# def path_divider(path):
#     part_path = path.split('/')
#     return part_path
def get_terminal(key):
    tmp = key.split('/')
    terminal = tmp[len(tmp) - 1]
    return terminal

def find_busbar(G):
    for n in G.nodes:
        busbar = ""
        isbusbar = True
        for n2 in G.nodes:
            for i in range(len(G.nodes[n2].neighbors)):
                if G.nodes[n].name == G.nodes[n2].neighbors[i].get("neighbor").name:
                    isbusbar = False
            if len(G.nodes[n2].neighbors)>0:
                print(G.nodes[n2].neighbors[0].get("neighbor"))
            busbar = G.nodes[n].name
        if  isbusbar:
            G.rootName = busbar
            return busbar
    return "No Busbar"


def path_combine(paths): #combines path with terminal removed. Vertex name.
    combined_path = paths[0]
    for i in range(1,len(paths)-1):
        combined_path = combined_path + '/' + paths[i]
    return combined_path

def has_second_terminal(pathArray, key, terminal_2): #ikinci terminali varsa bara değildir
    for part in pathArray:
        if part != key:
            part_div = part.split('/')
            key_div = key.split('/')
            if path_combine(part_div) == path_combine(key_div) and terminal_2 == get_terminal(part):
                return True
    return False

def has_first_terminal(pathArray, key, terminal_1):
    for part in pathArray:
        if part != key:
            part_div = part.split('/')
            key_div = key.split('/')
            if path_combine(part_div) == path_combine(key_div) and terminal_1 == get_terminal(part):
                return True
    return False


def terminal_finder(pathArray):
    for part in pathArray:
        for part2 in pathArray:
            if part != part2:
                part_div = part.split('/')
                part2_div = part2.split('/')

                if path_combine(part_div) == path_combine(part2_div):
                    list_terminal = [part_div[len(part_div) - 1], part2_div[len(part_div) - 1]]
                    list_terminal.sort()
                    break
        else:
            continue
        break
    return list_terminal

def typeFinder(path):
    pathArr = path.split("/")
    type = pathArr[len(pathArr)-2]
    return type


def assign_layers(G, x):
    # array to store level of each node
    level = [None] * len(G.nodes)
    marked = [False] * len(G.nodes)
    # create a queue
    que = queue.Queue()
    # enqueue element x
    que.put(x)
    # initialize level of source
    # node to 0
    #level[x] = 0
    G.nodes[x].layer = 1
    maxLayer = 1
    # marked it as visited
    #marked[x] = True
    # do until queue is empty
    maxXUnit = 1
    while (not que.empty()):
        # get the first element of queue
        x = que.get()
        # traverse neighbors of node x
        #for i in range(len(graph[x])):
        maxChildren = 1
        for i in G.nodes[x].neighbors:
            # b is neighbor of node x
            #b = graph[x][i]
            # if b is not marked already
            if i["neighbor"].layer == 0:
                if len(i["neighbor"].neighbors) > maxChildren:
                    maxChildren = len(i["neighbor"].neighbors)
                # enqueue b in queue
                que.put(i["neighbor"].name)
                # level of b is level of x + 1
                #level[b] = level[x] + 1
                i["neighbor"].layer = G.nodes[x].layer + 1
                if i["neighbor"].layer > maxLayer:
                    maxLayer = i["neighbor"].layer
        maxXUnit = maxXUnit * maxChildren
    # display all nodes and their levels
    print("Nodes", " ", "Level")
    for i in G.nodes:
        print(" ", G.nodes[i].name, " --> ", G.nodes[i].layer)
    return maxXUnit, maxLayer

def empthyNodes(G, maxLayer):
    finished = False
    dummyNodes = []
    for n in G.nodes:
        if len(G.nodes[n].neighbors) == 0 and G.nodes[n].layer <  maxLayer:
            dummyNodes.append(G.nodes[n].name)
            finished = True
    print(len(dummyNodes))
    for i in range(len(dummyNodes)):
        G.add_vertex(dummyNodes[i] + "jr", "Dummy", x=0, y=0, isdummy=True, layer=G.nodes[dummyNodes[i]].layer + 1)
        G.add_edge(dummyNodes[i] + "jr_edge", dummyNodes[i], dummyNodes[i] + "jr")
    return finished


def findK(node, k):
    node.xUnit = k
    print(str(node.name) + " " + str(k))
    if len(node.neighbors) > 0:
        for n in node.neighbors:
            findK(n["neighbor"], k * len(node.neighbors))

def find_max_children(G, maxlayer):
    maxChildren = 1
    for layer in range(1, maxlayer):
        filteredSortedNodes = filterlayers(G, layer)
        layerMaxChild = 0
        for i in range(len(filteredSortedNodes)):
            if layerMaxChild < len(filteredSortedNodes[i].neighbors):
                layerMaxChild = len(filteredSortedNodes[i].neighbors)
        maxChildren *= layerMaxChild
    return maxChildren

def sortHelper(e):
    return e.name

def filterlayers(G, layer):
    filterednodes = []
    for node in G.nodes:
        if G.nodes[node].layer == layer:
            filterednodes.append(G.nodes[node])
    if(len(filterednodes)):
        filterednodes.sort(key=sortHelper)
        return filterednodes
    else:
        return []

def assignCoords(G, m, l, w, h, W, H): #m-> max children, w en yakın iki nod arasındaki boşluk
    for layer in range(1,l + 1):
        filteredSortedNodes = filterlayers(G,layer)
        for i in range(len(filteredSortedNodes)):
            for j in range(i):
                filteredSortedNodes[i].x += (1/filteredSortedNodes[j].xUnit)
            filteredSortedNodes[i].x += ( 1/ (2*filteredSortedNodes[i].xUnit))
            filteredSortedNodes[i].x = (filteredSortedNodes[i].x*(w*m)) + W #W offset olarak
            filteredSortedNodes[i].y = layer*h + H #H offset olarak mesela CB nin genişliği
    G.nodes[find_busbar(G)].x = W
    G.nodes[find_busbar(G)].busbarsize = m * w


if __name__ == "__main__":
    G , root = build_graph()
    maxXUnit, maxLayer = assign_layers(G, root[0])
    while empthyNodes(G, maxLayer):
        pass

    findK(G.nodes[root[0]], 1)
    print(maxXUnit)
    print(maxLayer)
    print(find_max_children(G,maxLayer))
    assignCoords(G, find_max_children(G,maxLayer), maxLayer, 500, 350, 2000, 2000)
    for n in G.nodes:
        print(G.nodes[n].name + " X:" + str(G.nodes[n].x) + " Y:" + str(G.nodes[n].y))
    #try:
        # path = input("Enter your file path: ")
        #G= build_graph()
    print(G.edges)
    print(find_busbar(G))
        #print(G.nodes)
    #except :
       # pass